# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions
import datetime, re

class Patient(models.Model):
    _name = 'dental.patient'

    name = fields.Char(string='Nombre', required=True)
    last_name = fields.Char(string='Apellidos', required=True)
    dni = fields.Char(string='DNI',required=True)
    email = fields.Char(string='Email')
    phone = fields.Char(string='Celular')
    gender = fields.Selection(selection=[('M','Masculino'),('F','Femenino')],required=True)
    address = fields.Text(string='Dirección')
    birth_date = fields.Date(string='Fecha de Nacimiento',required=True)
    blood_type = fields.Selection(selection=[('A-','A Negativo'),('A+','A Positivo'),('B-','B Negativo'),('B+','B Positivo'),('AB-','AB Negativo'),('AB+','AB Positivo'),('O-','O Negativo'),('O+','O Positivo')], string='Grupo Sanguíneo')
    dental_records = fields.Text(string='Antecedentes bucales')
    clinic_history_id = fields.Many2one('dental.history', string='Historia Clínica', required=True)
    
    @api.constrains('phone')
    def _verify_number_phone(self):
        for rec in self:
            regex = r"^[0-9]{9}$"
            result = re.search(regex, rec.phone)
            if not result:
                raise exceptions.ValidationError('El número de teléfono debe tener 9 dígitos numéricos')

    @api.constrains('email')    
    def verify_format_email(self):
        for rec in self:
            regex = r"/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i"
            result = re.search(regex, rec.email)
            if not result:
                raise exceptions.ValidationError('La dirección de E-mail no contiene el formato adecuado.')
    
            

class Dentist(models.Model):
    _name = 'dental.dentist'

    name = fields.Char(string='Nombre', required=True)
    last_name = fields.Char(string='Apellidos', required=True)
    dni = fields.Char(string='DNI',required=True)
    email = fields.Char(string='Email')
    phone = fields.Char(string='Celular')
    gender = fields.Selection(selection=[('M','Masculino'),('F','Femenino')],required=True)
    address = fields.Text(string='Dirección')
    birth_date = fields.Date(string='Fecha de Nacimiento', required=True)
    account_number = fields.Char(string='Número de cuenta')
    contract_date = fields.Date(string='Fecha de contratación', required=True)
    professional_identify = fields.Char(string='Identificación profesional',required=True)

    @api.constrains('phone')
    def _verify_number_phone(self):
        for rec in self:
            regex = r"^[0-9]{9}$"
            result = re.search(regex, rec.phone)
            if not result:
                raise exceptions.ValidationError('El número de teléfono debe tener 9 dígitos numéricos')

class Assistant(models.Model):
    _name = 'dental.assistant'

    name = fields.Char(string='Nombre', required=True)
    last_name = fields.Char(string='Apellidos', required=True)
    dni = fields.Char(string='DNI',required=True)
    email = fields.Char(string='Email')
    phone = fields.Integer(string='Celular')
    gender = fields.Selection(selection=[('M','Masculino'),('F','Femenino')],required=True)
    address = fields.Text(string='Dirección')
    birth_date = fields.Date(string='Fecha de Nacimiento',required=True)
    account_number = fields.Char(string='Número de cuenta')
    contract_date = fields.Date(string='Fecha de contratación',required=True)
    payment_date =  fields.Date(string='Fecha de pago')

class Secretary(models.Model):
    _name = 'dental.secretary'

    name = fields.Char(string='Nombre', required=True)
    last_name = fields.Char(string='Apellidos', required=True)
    dni = fields.Char(string='DNI',required=True)
    email = fields.Char(string='Email')
    phone = fields.Integer(string='Celular')
    gender = fields.Selection(selection=[('M','Masculino'),('F','Femenino')],required=True)
    address = fields.Text(string='Dirección')
    birth_date = fields.Date(string='Fecha de Nacimiento',required=True)
    account_number = fields.Char(string='Número de cuenta')
    contract_date = fields.Date(string='Fecha de contratación',required=True)
    payment_date =  fields.Date(string='Fecha de pago')

class Treatment(models.Model):
    _name = 'dental.treatment'

    name = fields.Char(string='Tratamiento')
    category_id = fields.Many2one('dental.category_treatment', ondelete='set null', string='Categoría', required = True)
    internal_code =fields.Char(string='Código Interno', required=True)
    price = fields.Float(string='Precio', digits=(4,2) ,required=True)
    active = fields.Boolean(string='Activo', default=True)

class CategoryTreatment(models.Model):
    _name = 'dental.category_treatment'

    name = fields.Char(string='Nombre', required=True)
    description = fields.Text(string='Descripción')

class Voucher(models.Model):
    _name = 'dental.voucher'

    name = fields.Char(string='Boleta', required=True)
    code = fields.Char(string='Código', required=True)
    date = fields.Date(string='Fecha de emisión', required=True)
    status = fields.Char(string='Estado', domain=['|',('status','=','Cancelado'),('status','=','Pendiente')],required=True)
    observations = fields.Text(string='Observaciones')

class DetailTreatmentVoucher(models.Model):
    _name = 'dental.detail'

    name = fields.Char(string='Detalle Boleta')
    treatment_id = fields.Many2one('dental.treatment', string='Tratamiento', required=True)
    voucher_id = fields.Many2one('dental.voucher', string='Voucher', required=True)
    sub_total = fields.Float(string='Sub-total', digits=(4,2), required=True)

class ClinicalHistory(models.Model):
    _name = 'dental.history'

    name = fields.Char(string='Historia Clínica', required=True)
    date_registered = fields.Date(string='Fecha de registro', default=fields.Date.today)
    observations = fields.Text(string='Observaciones')

    #validar que la fecha de registro sea igual o posterior a hoy
    @api.constrains('date_registered')
    def _validate_date_registered(self):
        for rec in self:
            if rec.date_registered < datetime.date.today():
                raise exceptions.ValidationError('La fecha de registro debe ser igual o posterior a la fecha actual')

class Appointment(models.Model):
    _name = 'dental.appointment'

    #name = fields.Char('')
    patient_id = fields.Many2one('dental.patient', string='Paciente', ondelete= 'set null')
    dentist_id = fields.Many2one('dental.dentist', string='Dentista', ondelete='set null')
    treatment_id = fields.Many2one('dental.treatment', string='Tratamiento', ondelete='set null')
    assistant_id = fields.Many2one('dental.assistant', string='Asistente')
    secretary_id = fields.Many2one('dental.secretary', string='Secretaria')
    date_appointment = fields.Date(string='Fecha de cita', default=fields.Date.today)
    turn_id = fields.Many2one('dental.turn', string='Turno')
    type_appointment = fields.Selection(selection=[('O','Ordinaria'),('E','Extraordinaria')],default='O')
    status = fields.Selection([('P','Pendiente'),('P','Postergada'),('R','Realizada'),('S','Suspendida')],default='P')
    require_assistant = fields.Boolean(string='Requiere asistente', default=False)

    @api.constrains('patient_id','dentist_id')
    def _verify_dentist_in_not_patient(self):
        for rec in self:
            if self.patient_id == self.dentist_id:
                raise exceptions.ValidationError('El dentista no puede ser el paciente en la misma cita.')

class Turn(models.Model):
    _name = 'dental.turn'

    name  = fields.Char(string='Inicio', store=True) 
    duration = fields.Integer(string='Duración (min)',default=30)
    start_hour = fields.Selection(selection=[('01','01'),('02','02'),('03','03'),('04','04'),('05','05'),('06','06'),('07','07'),('08','08'),('09','09'),('10','10'),('11','11'),('12','12')], string='Hora', default='12',required=True)
    start_minute = fields.Selection(selection=[('00','00'),('10','10'),('20','20'),('30','30'),('40','40'),('50','50')], string='Minuto', default='00',required=True)
    format_hour = fields.Selection(selection=[('am','AM'),('pm','PM')],string='Formato',default='am', required=True)
    #duration_hours = fields.Integer(string='Horas',default=0)
    #duration_minutes = fields.Integer(string='Minutos', default=0)

    @api.onchange('start_hour','start_minute','format_hour')
    def _set_abbreviation(self):
        
        if self.start_hour and self.start_minute and self.format_hour:
            self.name = self.start_hour +':'+ self.start_minute +':'+ self.format_hour
        else:
            self.name = '00:00:00'

    @api.constrains('name')
    def _verify_abbreviation_value(self):
        for rec in self:
            if not rec.name or rec.name == '00:00:00':
                raise exceptions.ValidationError("El valor para el turno no es válido.")

    




    # @api.depends('start_hour','start_minute','duration_hours','duration_minutes')
    # def _get_end_turn(self):
    #     for rec in self:
    #         minute = int(self.minute) 
    #         hour = int(self.hour)
    #         added_hour = int(self.added_hour)
    #         added_minutes = int(self.added_minutes)
    #         hour += added_hour
    #         minute += added_minutes 
                
    #         if minute >= 60:
    #             minute = minute % 60
    #             hour += 1
    #         if hour >= 13:
    #             hour = hour % 13    

    #         rec.end = str(hour) + ':' str(minute) + self.format_hour
        
        
                
         
# class odoo-dental-clinic(models.Model):
#     _name = 'odoo-dental-clinic.odoo-dental-clinic'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100